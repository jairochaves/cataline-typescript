// Decorators - Fazer anotação da versão da api

function setApiVersion(apiVersion: string) {
  return (constructor: any) => {
    return class extends constructor {
      version = apiVersion;
    };
  };
}

@setApiVersion('1.0.0')
class API {

}
@setApiVersion('2.0.0')
class API2 {

}
console.log(new API(), new API2())